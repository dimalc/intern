package com.example.intern;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.intern.Adapter.RecycleViewAdapter;
import com.example.intern.Model.User;
import com.example.intern.Utilities.JsonUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ProfileActivity extends AppCompatActivity {

    Intent recivedIntent;
    public String TAG="ProfileActivity";
    String BASE_URL="https://reqres.in/api/users/";
    User user;
    TextView uId,uFirstLastName,uEmail;
    ImageView avatar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        recivedIntent=getIntent();
        if (recivedIntent!=null){
            user=(User) recivedIntent.getSerializableExtra("USER");
        }
        uId=findViewById(R.id.uId);
        uFirstLastName=findViewById(R.id.firstLastName);
        uEmail=findViewById(R.id.uEmail);
        avatar=findViewById(R.id.avatar);
        updateUser(user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void updateUser(User u){
        if (user!=null){
        uId.setText(""+u.getId());
        uFirstLastName.setText(u.getFirtName()+ " "+u.getLastName());
        uEmail.setText(u.getEmail());
        Picasso.get()
                .load(u.getAvatar())
                .into(avatar);
        }
    }
    private void getUser(String number) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(BASE_URL+number,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d(TAG ,"Json Succes"+response.toString());
                    user= JsonUtil.getUser(response);
                    updateUser(user);


                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.d(TAG ,"Json Failed");
            }
        });
    }

    public void prevUser(View view) {
        if (user.getId()==1){
            Toast.makeText(this,"No previus user !",Toast.LENGTH_LONG).show();
        }else {
            int number=user.getId()-1;
            getUser(""+number);
        }
    }
    public void nextUser(View view) {
        if (user.getId()==12){
            Toast.makeText(this,"No next user !",Toast.LENGTH_LONG).show();
        }else {
            int number=user.getId()+1;
            getUser(""+number);

        }
    }

    public void scaleImage(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        dialog.setContentView(R.layout.dialog);
        ImageView imageView=dialog.findViewById(R.id.rescaleImage);
        Picasso.get()
                .load(user.getAvatar())
                .into(imageView);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(true);
            dialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
