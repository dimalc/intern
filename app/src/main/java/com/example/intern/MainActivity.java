package com.example.intern;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.example.intern.Fragments.MainFragment;
import com.example.intern.Model.User;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout=findViewById(R.id.frameLayout);
        fragmentManager=getSupportFragmentManager();
        addMainFragment();

    }
    private void addMainFragment(){
        MainFragment mainFragment=new MainFragment();
        fragmentManager.beginTransaction().add(R.id.frameLayout,
                mainFragment).disallowAddToBackStack().commit();;
    }


}
