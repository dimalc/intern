package com.example.intern.Model;

import java.io.Serializable;

public class User implements Serializable {

    private int id;
    private String email,firstName,lastName,avatar;

    public User(int id, String email, String firtName, String lastName, String avatar) {
        this.id = id;
        this.email = email;
        this.firstName = firtName;
        this.lastName = lastName;
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirtName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAvatar() {
        return avatar;
    }
}
