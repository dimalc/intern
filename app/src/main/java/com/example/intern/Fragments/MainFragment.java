package com.example.intern.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.intern.Adapter.RecycleViewAdapter;
import com.example.intern.Model.User;
import com.example.intern.R;
import com.example.intern.Utilities.JsonUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainFragment extends Fragment {

    public static final String TAG="MainActivity";
    public static final String BASE_URL="https://reqres.in/api/users?per_page=12";
    List<User> allUsers=new ArrayList<>();
    RecyclerView recyclerView;
    RecycleViewAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView =view.findViewById(R.id.recyclerView);
        getUsers();
        return view;
    }



    private void getUsers() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(BASE_URL,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d(TAG,"Json Succes"+response.toString());
                    allUsers= JsonUtil.getUsers(response);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    adapter = new RecycleViewAdapter(getContext(),allUsers);
                    recyclerView.setAdapter(adapter);

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.e(TAG ,"Json Failed");
            }
        });
    }

}

