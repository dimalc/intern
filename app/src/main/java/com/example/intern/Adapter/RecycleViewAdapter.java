package com.example.intern.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.intern.Model.User;
import com.example.intern.ProfileActivity;
import com.example.intern.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {

    Context context;
    List<User> u;

    public RecycleViewAdapter(Context c, List<User> users){
        this.u=users;
        this.context=c;
    }

    public Context getContext(){
        return context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        User user=u.get(i);

        viewHolder.firstName.setText(u.get(i).getFirtName());
        viewHolder.lastName.setText(u.get(i).getLastName());
        Picasso.get()
                .load(user.getAvatar())
                .into(viewHolder.avatar);

    }

    @Override
    public int getItemCount() {
        return u.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView avatar;
        TextView firstName,lastName;

        ViewHolder(View view){
            super(view);
            firstName=view.findViewById(R.id.first_name);
            lastName=view.findViewById(R.id.last_name);
            avatar=view.findViewById(R.id.avatar);
            view.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {

            User user = u.get(getAdapterPosition());
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            intent.putExtra("USER", user);
            getContext().startActivity(intent);

        }
    }
}


