package com.example.intern.Utilities;

import com.example.intern.Model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public final class JsonUtil {

    private JsonUtil(){}


    public static List<User> getUsers(JSONObject object) throws  JSONException{

        List<User> userList= new ArrayList<>();
        JSONArray data= object.getJSONArray("data");

        for (int i=0;i<data.length();i++){
            JSONObject userObject=data.getJSONObject(i);

            int id=userObject.getInt("id");
            String email=userObject.getString("email");
            String firstName=userObject.getString("first_name");
            String lastName=userObject.getString("last_name");
            String avatar=userObject.getString("avatar");

            User user=new User(id,email,firstName,lastName,avatar);
            userList.add(user);

        }
        return userList;
    }
    public static User getUser(JSONObject object) throws JSONException {
        User user;
        JSONObject data = object.getJSONObject("data");
        int id = data.getInt("id");
        String email = data.getString("email");
        String firstname = data.getString("first_name");
        String lastname = data.getString("last_name");
        String avatar = data.getString("avatar");

        user = new User(id, email, firstname, lastname, avatar);
        return user;
    }

}
